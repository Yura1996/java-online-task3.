package Animals;

public class App {
    public static void main(String[] args) {
        Shark sh=new Shark();
        sh.eat();
        sh.sleep();
        sh.swim();
        System.out.println();
        Parrot parrot = new Parrot();
        parrot.eat();
        parrot.fly();
        parrot.sleep();
        System.out.println();
        Eagle eagle = new Eagle();
        eagle.eat();
        eagle.fly();
        eagle.sleep();
    }
}

package Animals;

public abstract class Bird extends Animal {
    public void fly (){
        System.out.println("Bird is flying");
    }
}

package Animals;

public abstract class Animal {
    public void sleep(){
        System.out.println("Animal is sleeping");
    }
    public void eat(){
        System.out.println("Animal eats everything ");
    }
}

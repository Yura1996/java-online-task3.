package Animals;

public class Parrot extends Bird {
    @Override
    public void eat() {
        System.out.println("Parrot eats plants");
    }
}

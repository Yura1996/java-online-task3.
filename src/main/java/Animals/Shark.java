package Animals;

public class Shark extends Fish  {
    @Override
    public void eat() {
        System.out.println("Shark eats meat");
    }
}

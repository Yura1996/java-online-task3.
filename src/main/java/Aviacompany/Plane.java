package Aviacompany;



    public abstract class Plane {
        private int weight;
        private String modelName;
        private int flightRange;
        private double engineVolume;
        private double fuel;

        public  int getPeopleCapacity(Plane plane){
            return plane.getWeight() / 90;
        }
        public int getTotalCapacity(Plane plane) {
            return plane.getWeight() / 4 ;
        }

        public Plane(int weight, String modelName, int flightRange, double engineVolume, double fuel) {
            this.weight = weight;
            this.modelName = modelName;
            this.flightRange = flightRange;
            this.engineVolume = engineVolume;
            this.fuel = fuel;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        public int getFlightRange() {
            return flightRange;
        }

        public void setFlightRange(int flightRange) {
            this.flightRange = flightRange;
        }

        public double getEngineVolume() {
            return engineVolume;
        }

        public void setEngineVolume(double engineVolume) {
            this.engineVolume = engineVolume;
        }

        public double getFuel() {
            return fuel;
        }

        public void setFuel(double fuel) {
            this.fuel = fuel;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        @Override
        public String toString() {
            return "Plane{" +
                    "weight=" + weight +
                    ", modelName='" + modelName + '\'' +
                    ", flightRange=" + flightRange +
                    ", engineVolume=" + engineVolume +
                    ", fuel=" + fuel +
                    '}';
        }
    }



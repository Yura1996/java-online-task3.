package Aviacompany;


    import java.util.Comparator;

public class PlaneComparator implements Comparator<Plane> {

    //--------------сортування літаків по дальості польоту--------------------
    public int compare(Plane o1, Plane o2) {
        if (o1.getFlightRange() == o2.getFlightRange()) {
            return 0;
        }else if (o1.getFlightRange() > o2.getFlightRange()) {
            return 1;
        }else {
            return -1;
        }
    }
}



package Aviacompany;


    import java.util.ArrayList;
import java.util.List;

public class Aviacompany {
    public static List<Plane> list = new ArrayList<Plane>();

    static {
        list.add(new Boeing(4000,"NCV-923", 8000,122.4,1424124.2));
        list.add(new Boeing(4500,"BVG-183", 9700,242.4,1424254.2));
        list.add(new Airbus(34000,"LMO-124", 7000,2122.4,1422424.2));
        list.add(new Boeing(5000,"NCW-961", 7600,722.4,1424234.2));
        list.add(new Airbus(4500,"CXV-235", 6000,2322.4,14242564.2));
        list.add(new Boeing(6500,"MKO-463", 9800,2122.4,1424264.2));
        list.add(new Airbus(4000,"PLQ-136", 8000,2422.4,1424234.2));
        list.add(new Boeing(1000000,"SUPERPLANE-666", 1000000,100000.0,1000000));
    }

    public static void findPlaneByRange(int range) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getFlightRange() == range) {
                System.out.println(list.get(i));
            }

        }
    }


}


package Figures;

public class Rectangle extends Shape implements Comparable<Rectangle> {
    private double a;
    private double b;

    public Rectangle(double b, String color, double a) {
        super(color);
        this.b = b;
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }


    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }


    public double perimeter() {
        return 2 * a + 2 * b;
    }

    public double square() {
        return a * b;
    }

    public int compareTo(Rectangle o) {
        if (this.a > o.a)
            return 1;
        else if (this.a < o.b)
            return -1;
        else if (this.b > o.b)
            return 1;
        else if (this.b < o.b)
            return -1;
        return 0;
    }

    @Override
    public String toString() {
        return "Rectangle : " + a + " " + b + "; squere: " + square() + super.toString();
    }
}

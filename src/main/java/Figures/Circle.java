package Figures;

public class Circle extends Shape implements Comparable<Circle> {
    private double radius;

    public Circle(String color, double radius) {
        super(color);
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    public double square() {
        return Math.PI*radius*radius;
    }

    public int compareTo(Circle o) {
        if (this.radius > o.radius)
            return 1;
        else if (this.radius < o.radius)
            return -1;
        return 0;
    }

    @Override
    public String toString() {
        return "Circle : " + radius+ "; square: " + square() + super.toString();
    }
}

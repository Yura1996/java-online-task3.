package Figures;

public class Triangle extends Shape implements Comparable<Triangle>{
    private double a;
    private double b;
    private double c;
    private double p;
    public Triangle (double b, String color, double a,double c) {
        super(color);
        this.b = b;
        this.a = a;
        this.c = c;
        this.p=perimeter();
    }

    public double perimeter() {
        return a+b+c;
    }

    public double square() {
        return Math.sqrt(p*(p-a)*(p-b)*(p-c)) ;
    }

    public int compareTo(Triangle o) {
        if (this.a > o.a)
            return 1;
        else if (this.a < o.b)
            return -1;
        else if (this.b > o.b)
            return 1;
        else if (this.b < o.b)
            return -1;
        else if (this.c > o.c)
            return 1;
        else if (this.c < o.c)
            return -1;
        return 0;

    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }
    @Override
    public String toString() {
        return "Triangle : " + a + " " + b + " " + c + "; square: " + square() + super.toString();
    }
}

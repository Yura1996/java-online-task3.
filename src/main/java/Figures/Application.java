package Figures;

import java.util.Arrays;

public class Application {
    public static void main(String[] args) {
        Rectangle rec = new Rectangle(5,"Red", 7);
        Rectangle rec2 = new Rectangle(4,"Yellow", 6);
        Rectangle rec3 = new Rectangle(8,"Blue", 9);
        Circle circ=new Circle("Red", 10);
        Circle circ2=new Circle("Blue", 12) ;
        Circle circ3=new Circle("Green", 14) ;
        Triangle trian=new Triangle(12, "Orange", 13,14);
        Triangle trian2=new Triangle(7, "Purple", 10,11);
        Triangle trian3=new Triangle(9, "Grey", 8,6);
        Rectangle[] rectangles = new Rectangle[3];
        rectangles[0] = rec;
        rectangles[1] = rec2;
        rectangles[2] = rec3;
        Arrays.sort(rectangles);
        Circle[] circles = new Circle[3];
        circles[0] = circ;
        circles[1] = circ2;
        circles[2] = circ3;
        Arrays.sort(circles);
        Triangle[] triangles = new Triangle[3];
        triangles[0] = trian;
        triangles[1] = trian2;
        triangles[2] = trian3;
        Arrays.sort(triangles);

        for (Rectangle r: rectangles) {
            System.out.println(r);
        }

        for (Circle c: circles) {
            System.out.println(c);
        }

        for (Triangle t: triangles) {
            System.out.println(t);
        }
    }
}
